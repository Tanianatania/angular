import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Project } from '../../models/project';
//  import { UaDatePipe } from 'src/app/pipes/ua-date.pipe';

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.css'], 
  // providers: [UaDatePipe]
})
export class ProjectDetailsComponent implements OnInit {

  @Input() item: Project;
  @Input('index') itemIndex: number;
  @Input() titleColor: string;
  @Output() itemSelected = new EventEmitter<number>();

  // constructor(uaDate: UaDatePipe) {

  // }

  ngOnInit() {
  }

  projectSelected() {
    this.itemSelected.emit(this.itemIndex);
  }
}
