import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { PipeModule } from '../pipes/pipe.module';

import { ProjectsListComponent } from './projects-list/projects-list.component';
import { ProjectDetailsComponent } from './project-details/project-details.component';
import { ProjectPageComponent } from './project-page/project-page.component';
import { ProjectCreateComponent } from './project-create/project-create.component';


@NgModule({
  declarations: [
    ProjectsListComponent,
    ProjectDetailsComponent,
    ProjectPageComponent,
    ProjectCreateComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule, 
    PipeModule
  ],
  exports: []
})
export class ProjectsModule { }
