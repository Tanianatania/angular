import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Project } from '../../models/project';

@Component({
  selector: 'app-projects-list',
  templateUrl: './projects-list.component.html',
  styleUrls: ['./projects-list.component.css']
})

export class ProjectsListComponent implements OnInit {

  constructor() { }

  @Input('projects') projectsList: Project[];
  @Output() itemSelected = new EventEmitter<number>();

  ngOnInit() {
  }

  projectSelected(index: number) {
    this.itemSelected.emit(index);
  }
}