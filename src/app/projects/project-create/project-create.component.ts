import { Component, OnInit, OnChanges, EventEmitter, Input, Output, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Project } from '../../models/project';
import { ComponentCanDeactivate } from 'src/app/guards/exit.guard';
import { Observable } from 'rxjs/internal/Observable';

@Component({
  selector: 'app-project-create',
  templateUrl: './project-create.component.html',
  styleUrls: ['./project-create.component.css']
})
export class ProjectCreateComponent implements OnInit, OnChanges, ComponentCanDeactivate {

  @Input() project: Project = {} as Project;
  @Output() projectChange = new EventEmitter<Project>();
  projectForm: FormGroup;

  saved: boolean = false;

  canDeactivate(): boolean | Observable<boolean> {

    if (!this.saved) {
      return confirm("Do tou want leave page?");
    }
    else {
      return true;
    }
  }

  ngOnInit() {
    console.log(this.project);
    this.projectForm = new FormGroup({
      'id': new FormControl(this.project.id),
      'name': new FormControl(this.project.name, [
        Validators.required,
        Validators.minLength(4)
      ]),
      'description': new FormControl(this.project.description),
      'createdAt': new FormControl(this.project.createdAt),
      'deadline': new FormControl(this.project.deadline, [
        Validators.required
      ]),
      'authorId': new FormControl(this.project.authorId, [
        Validators.required
      ]),
      'teamId': new FormControl(this.project.teamId, [
        Validators.required
      ]),
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes)
    if (changes.project && this.projectForm) {
      this.projectForm.setValue(changes.project.currentValue)
    }
  }

  saveProject() {
    let newProject = this.projectForm.value
    newProject.createAt = new Date();
    this.projectChange.emit(newProject);
    this.projectForm.reset();
    this.saved = true;
  }

  revert() {
    this.projectForm.reset();
  }
}
