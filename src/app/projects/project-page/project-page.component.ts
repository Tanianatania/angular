import { Component, OnInit } from '@angular/core';
import { ProjectService } from '../../services/project/project.service';
import { Project } from '../../models/project';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-project-page',
  templateUrl: './project-page.component.html',
  styleUrls: ['./project-page.component.css']
})
export class ProjectPageComponent implements OnInit {

  hideCreateProject = true;
  projects: Project[] = [];
  selectedProject: Project = {} as Project;
  selectedProjectIndex: number = -1;
  constructor(private projectService: ProjectService) { }

  ngOnInit() {
    this.projectService.getProjects().pipe(map(x => {
      x = x.map(b => {
        b.createdAt = new Date(b.createdAt);
        b.deadline = new Date(b.deadline)
        if (!b.description) {
          b.description = "No description";
        }
        return b;
      })
      return x;
    })).subscribe(x => {
      this.projects = x;
    }, (error) => {
      console.log(error)
    });
  }

  showEdit() {
    if (this.hideCreateProject) {
      this.selectedProjectIndex = -1;
    }
    this.selectedProject = {
      id:null,
      name: '',
      description: '',
      createdAt: null,
      deadline: null,
      authorId: null,
      teamId: null
    } as Project;
    this.hideCreateProject = !this.hideCreateProject;
  }

  save(newProject: Project) {
    console.log(newProject);
    if (this.selectedProjectIndex === -1) {
      this.projectService.createProject(newProject);
      this.projects.push(newProject);
    } else {
      this.projectService.updateProject(newProject);
      this.projects[this.selectedProjectIndex] = newProject;    
    }
    this.selectedProjectIndex = -1;
    this.hideCreateProject = true;
  }

  select(index: number) {
    let Project = this.projects[index];
    console.log(Project);
    this.selectedProject = {
      id:Project.id,
      authorId: Project.authorId,
      teamId: Project.teamId,
      description: Project.description,
      deadline: Project.deadline || new Date(),
      createdAt: Project.createdAt || Date.now,
      name: Project.name
    } as Project;
    this.selectedProjectIndex = index;
    this.hideCreateProject = false;
  }

}
