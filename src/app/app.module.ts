import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';
import {registerLocaleData} from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import localUk from '@angular/common/locales/uk'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from '../app/app-root/app.component';
import { NavigationComponent } from './navigation/navigation.component';

import {ProjectsModule} from './projects/projects.module';
import {TasksModule} from './task/tasks.module';
import { PipeModule } from './pipes/pipe.module';
import { UserModule } from './user/user.module';
import {TeamModule} from './team/team.module';

registerLocaleData(localUk);

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    TranslateModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    ProjectsModule,
    TasksModule ,
    UserModule,
    TeamModule,
    PipeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
