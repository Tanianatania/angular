import { Component, OnInit } from '@angular/core';
import { TeamService } from '../../services/team/team.service';
import { Team } from '../../models/team';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-team-page',
  templateUrl: './team-page.component.html',
  styleUrls: ['./team-page.component.css']
})
export class TeamPageComponent implements OnInit {

  hideCreateTeam = true;
  teams: Team[] = [];
  selectedTeam: Team = {} as Team;
  selectedTeamIndex: number = -1;
  constructor(private teamService: TeamService) { }

  ngOnInit() {
    this.teamService.getTeams().pipe(map(x => {
      x = x.map(b => {
        b.createdAt = new Date(b.createdAt);
        b.createdAt = new Date(b.createdAt)
        return b;
      })
      return x;
    })).subscribe(x => {
      this.teams = x;
    }, (error) => {
      console.log(error)
    });
  }

  showEdit() {
    if (this.hideCreateTeam) {
      this.selectedTeamIndex = -1;
    }
    this.selectedTeam = {
      id:null,
      name: '',
      createdAt: null,
    } as Team;
    this.hideCreateTeam = !this.hideCreateTeam;
  }

  save(newTeam: Team) {
    console.log(newTeam);
    if (this.selectedTeamIndex === -1) {
      this.teamService.createTeam(newTeam);
      this.teams.push(newTeam);
    } else {
      this.teamService.updateTeam(newTeam);
      this.teams[this.selectedTeamIndex] = newTeam;    
    }
    this.selectedTeamIndex = -1;
    this.hideCreateTeam = true;
  }

  select(index: number) {
    let Team = this.teams[index];
    console.log(Team);
    this.selectedTeam = {
      id:Team.id,
      createdAt: Team.createdAt || Date.now,
      name: Team.name
    } as Team;
    this.selectedTeamIndex = index;
    this.hideCreateTeam = false;
  }

}
