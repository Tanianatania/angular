import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {Team} from '../../models/team';

@Component({
  selector: 'app-team-list',
  templateUrl: './team-list.component.html',
  styleUrls: ['./team-list.component.css']
})
export class TeamListComponent implements OnInit {

  constructor() { }

  @Input('teams') teamsList: Team[];
  @Output() itemSelected = new EventEmitter<number>();

  ngOnInit() {
  }

  teamSelected(index: number) {
    this.itemSelected.emit(index);
  }

}
