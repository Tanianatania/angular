import { Component, OnInit, OnChanges, EventEmitter, Input, Output, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Team } from '../../models/team';

@Component({
  selector: 'app-team-create',
  templateUrl: './team-create.component.html',
  styleUrls: ['./team-create.component.css']
})
export class TeamCreateComponent implements OnInit {

  @Input() team: Team = {} as Team;
  @Output() teamChange = new EventEmitter<Team>();
  teamForm: FormGroup;

  constructor() { }

  ngOnInit() {
    console.log(this.team);
    this.teamForm = new FormGroup({
      'id': new FormControl(this.team.id),
      'name': new FormControl(this.team.name, [
        Validators.required,
        Validators.minLength(4)
      ]),
      'createdAt': new FormControl(this.team.createdAt, [
        Validators.required
      ]),
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes)
    if (changes.team && this.teamForm) {
      this.teamForm.setValue(changes.team.currentValue)
    }
  }

  saveTeam() {
    let newTeam = this.teamForm.value as Team;
    newTeam.id=0;
    newTeam.createdAt=new Date();
    console.log(newTeam);
    this.teamChange.emit(newTeam);
    this.teamForm.reset();
  }

  revert() {
    this.teamForm.reset();
  }
}
