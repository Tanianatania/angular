import { Component, OnInit, Input, Output, EventEmitter, OnDestroy, OnChanges, SimpleChanges } from '@angular/core';
import {Team} from '../../models/team';

@Component({
  selector: 'app-team-details',
  templateUrl: './team-details.component.html',
  styleUrls: ['./team-details.component.css']
})
export class TeamDetailsComponent implements OnInit {

  constructor() { }

  @Input() item: Team;
  @Input('index') itemIndex: number;
  @Input() titleColor: string;
  @Output() itemSelected =  new EventEmitter<number>();


  ngOnInit() {
  }

  teamSelected() {
    this.itemSelected.emit(this.itemIndex);
  }

}
