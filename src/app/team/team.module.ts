import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeamPageComponent } from './team-page/team-page.component';
import { TeamListComponent } from './team-list/team-list.component';
import { TeamDetailsComponent } from './team-details/team-details.component';
import { TeamCreateComponent } from './team-create/team-create.component';
import { PipeModule } from '../pipes/pipe.module';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    TeamPageComponent,
    TeamListComponent,
    TeamDetailsComponent,
    TeamCreateComponent
  ],
  imports: [
    CommonModule,
    PipeModule, 
    ReactiveFormsModule
  ]
})
export class TeamModule { }
