import { Directive, HostBinding } from '@angular/core';

@Directive({
  selector: '[appTaskStateStarted]'
})
export class TaskStateStartedDirective {

  @HostBinding ('class.isStarted') isFinished=true;

}
