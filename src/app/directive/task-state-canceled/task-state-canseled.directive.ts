import { Directive, HostBinding } from '@angular/core';

@Directive({
  selector: '[appTaskStateCanseled]'
})
export class TaskStateCanseledDirective {

  @HostBinding ('class.isCanseled') isFinished=true;

}
