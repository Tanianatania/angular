import { Directive, HostBinding } from '@angular/core';

@Directive({
  selector: '[appTaskStateFinished]'
})
export class TaskStateFinishedDirective {

  @HostBinding ('class.isFinished') isFinished=true;
}
