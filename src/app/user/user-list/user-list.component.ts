import {User} from '../../models/user';
import { Component, OnInit , Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  constructor() { }

  @Input('users') usersList: User[];
  @Output() itemSelected = new EventEmitter<number>();

  ngOnInit() {
  }

  userSelected(index: number) {
    this.itemSelected.emit(index);
  }

}
