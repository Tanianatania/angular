import { Component, OnInit, OnChanges, EventEmitter, Input, Output, SimpleChanges } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { User } from '../../models/user';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.css']
})
export class UserCreateComponent implements OnInit {

  @Input() user: User = {} as User;
  @Output() userChange = new EventEmitter<User>();
  userForm: FormGroup;

  constructor() { }

  ngOnInit() {
    this.userForm = new FormGroup({
      'id': new FormControl(this.user.id),
      'firstName': new FormControl(this.user.firstName, [
        Validators.required,
        Validators.minLength(4)
      ]),
      'lastName': new FormControl(this.user.lastName, [
        Validators.required,
        Validators.minLength(4)
      ]),
      'email':new FormControl(this.user.email,[
        Validators.email
      ]),
      'registeredAt': new FormControl(this.user.registeredAt),
      'birthday': new FormControl(this.user.birthday, [
        Validators.required
      ]),
      'teamId': new FormControl(this.user.teamId, [
        Validators.required
      ]),
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes)
    if (changes.user && this.userForm) {
      this.userForm.setValue(changes.user.currentValue)
    }
  }

  saveUser() {
    let newUser = this.userForm.value
    newUser.registeredAt = new Date();
    this.userChange.emit(newUser);
    this.userForm.reset();
  }

  revert() {
    this.userForm.reset();
  }
}
