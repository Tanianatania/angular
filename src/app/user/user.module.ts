import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PipeModule } from '../pipes/pipe.module';
import { ReactiveFormsModule } from '@angular/forms';

import { UserPageComponent } from './user-page/user-page.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import { UserCreateComponent } from './user-create/user-create.component';




@NgModule({
  declarations: [
    UserPageComponent,
    UserListComponent,
    UserDetailsComponent,
    UserCreateComponent
  ],
  imports: [
    CommonModule, 
    PipeModule, 
    ReactiveFormsModule
  ],
  exports: []
})
export class UserModule { }
