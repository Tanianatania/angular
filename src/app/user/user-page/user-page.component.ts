import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user/user.service';
import { User } from '../../models/user';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.css']
})
export class UserPageComponent implements OnInit {

  hideCreateUser = true;
  users: User[] = [];
  selectedUser: User = {} as User;
  selectedUserIndex: number = -1;
  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userService.getUsers().pipe(map(x => {
      x = x.map(b => {
        b.birthday = new Date(b.birthday);
        b.registeredAt = new Date(b.registeredAt)
        if (!b.email) {
          b.email = "No email";
        }
        return b;
      })
      return x;
    })).subscribe(x => {
      this.users = x;
    }, (error) => {
      console.log(error)
    });
  }

  showEdit() {
    if (this.hideCreateUser) {
      this.selectedUserIndex = -1;
    }
    this.selectedUser = {
      id:null,
      firstName: '',
      lastName: '',
      registeredAt: null,
      birthday: null,
      email: null,
      teamId: null
    } as User;
    this.hideCreateUser = !this.hideCreateUser;
  }

  save(newUser: User) {
    console.log(newUser);
    if (this.selectedUserIndex === -1) {
      newUser.id=0;
      this.userService.createUser(newUser);
      this.users.push(newUser);
    } else {
      this.userService.updateUser(newUser);
      this.users[this.selectedUserIndex] = newUser;    
    }
    this.selectedUserIndex = -1;
    this.hideCreateUser = true;
  }

  select(index: number) {
    let User = this.users[index];
    this.selectedUser = {
      id:User.id,
      firstName: User.firstName,
      teamId: User.teamId,
      lastName: User.lastName,
      birthday: User.birthday || new Date(),
      registeredAt: User.registeredAt || Date.now,
      email: User.email
    } as User;
    this.selectedUserIndex = index;
    this.hideCreateUser = false;
  }

}
