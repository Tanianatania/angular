import { Component, OnInit , Input,EventEmitter, Output} from '@angular/core';
import {User} from '../../models/user';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {

  @Input() item: User;
  @Input('index') itemIndex: number;
  @Input() titleColor: string;
  @Output() itemSelected =  new EventEmitter<number>();

  ngOnInit() {
  }

  userSelected() {
    this.itemSelected.emit(this.itemIndex);
  }

}
