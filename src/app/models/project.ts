export interface Project {
  id: number;
  name: string;
  description : string;
  createdAt :Date;
  deadline : Date;
  authorId: Number;
  teamId: Number;
}