export interface Task{
  id:number;
  name: string;
  description: string;
  createdAt: Date;
  state: number;
  finishedAt: Date;
  projectId: number;
  performerId: number;
}