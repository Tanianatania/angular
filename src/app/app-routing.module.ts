import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectPageComponent } from './projects/project-page/project-page.component';
import { UserPageComponent } from './user/user-page/user-page.component';
import { TeamPageComponent } from './team/team-page/team-page.component';
import { TaskPageComponent } from './task/task-page/task-page.component';
import { ExitGuard } from './guards/exit.guard';
import { ProjectCreateComponent } from './projects/project-create/project-create.component';


const routes: Routes = [
  { path: 'projects', component: ProjectPageComponent },
  { path: 'projectscreate', component: ProjectCreateComponent },
  { path: 'users', component: UserPageComponent },
  { path: 'teams', component: TeamPageComponent },
  { path: 'tasks', component: TaskPageComponent },
  {
    path: 'projectscreate', 
    component: ProjectCreateComponent,
    canDeactivate: [ExitGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [ExitGuard],
})
export class AppRoutingModule { }
