import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Task } from '../../models/task'

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.css']
})
export class TaskListComponent implements OnInit {

  constructor() { }

  @Input('tasks') tasksList: Task[];
  @Output() itemSelected = new EventEmitter<number>();

  ngOnInit() {
  }

  taskSelected(index: number) {
    this.itemSelected.emit(index);
  }
}
