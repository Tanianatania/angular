import { Component, OnInit, Input, Output, EventEmitter, OnDestroy, OnChanges, SimpleChanges } from '@angular/core';
import { Task } from '../../models/task';

@Component({
  selector: 'app-task-details',
  templateUrl: './task-details.component.html',
  styleUrls: ['./task-details.component.css']
})
export class TaskDetailsComponent implements OnInit {

  @Input() item: Task;
  @Input('index') itemIndex: number;
  @Input() titleColor: string;
  @Output() itemSelected = new EventEmitter<number>();
  isFinished = false;
  isCanseled = false;
  isStarted=false;

  ngOnInit() {
    switch (this.item.state) {
      case 3:
        this.isFinished = true;
        break;
      case 2:
        this.isCanseled = true;
        break;
      case 1:
        this.isStarted=true;
      default:
        break; 
    }
  }

  taskSelected() {
    this.itemSelected.emit(this.itemIndex);
  }

}
