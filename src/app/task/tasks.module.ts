import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TaskPageComponent } from './task-page/task-page.component';
import { TaskListComponent } from './task-list/task-list.component';
import { TaskDetailsComponent } from './task-details/task-details.component';
import { TaskCreateComponent } from './task-create/task-create.component';
import { PipeModule } from '../pipes/pipe.module';
import { ReactiveFormsModule } from '@angular/forms';

import { TaskStateFinishedDirective } from '../directive/task-state-finished/task-state-finished.directive';
import { TaskStateCanseledDirective } from '../directive/task-state-canceled/task-state-canseled.directive';
import { TaskStateStartedDirective } from '../directive/task-sate-started/task-state-started.directive';



@NgModule({
  declarations: [
    TaskPageComponent,
    TaskListComponent,
    TaskDetailsComponent,
    TaskCreateComponent,
    TaskStateFinishedDirective,
    TaskStateCanseledDirective,
    TaskStateStartedDirective
  ],
  imports: [
    CommonModule, 
    PipeModule, 
    ReactiveFormsModule
  ]
})
export class TasksModule { }
