import { Component, OnInit } from '@angular/core';
import { TaskService } from '../../services/task/task.service';
import { Task } from '../../models/task';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-task-page',
  templateUrl: './task-page.component.html',
  styleUrls: ['./task-page.component.css']
})
export class TaskPageComponent implements OnInit {

  hideCreateTask = true;
  tasks: Task[] = [];
  selectedTask: Task = {} as Task;
  selectedTaskIndex: number = -1;
  constructor(private taskService: TaskService) { }

  ngOnInit() {
    this.taskService.getTasks().pipe(map(x => {
      x = x.map(b => {
        b.createdAt = new Date(b.createdAt);
        b.finishedAt = new Date(b.finishedAt)
        if (!b.description) {
          b.description = "No description";
        }
        return b;
      })
      return x;
    })).subscribe(x => {
      this.tasks = x;
    }, (error) => {
      console.log(error)
    });
  }

  showEdit() {
    if (this.hideCreateTask) {
      this.selectedTaskIndex = -1;
    }
    this.selectedTask = {
      id:null,
      state:null,
      name: '',
      description: '',
      createdAt: null,
      finishedAt: null,
      performerId: null,
      projectId: null
    } as Task;
    this.hideCreateTask = !this.hideCreateTask;
  }

  save(newTask: Task) {
    console.log(newTask);
    if (this.selectedTaskIndex === -1) {
      this.taskService.createTask(newTask);
      this.tasks.push(newTask);
    } else {
      this.taskService.updateTask(newTask);
      this.tasks[this.selectedTaskIndex] = newTask;    
    }
    this.selectedTaskIndex = -1;
    this.hideCreateTask = true;
  }

  select(index: number) {
    let Task = this.tasks[index];
    console.log(Task);
    this.selectedTask = {
      id:Task.id,
      state: Task.state,
      description: Task.description,
      performerId: Task.performerId,
      projectId: Task.performerId,
      finishedAt: Task.finishedAt || new Date(),
      createdAt: Task.createdAt || Date.now,
      name: Task.name
    } as Task;
    this.selectedTaskIndex = index;
    this.hideCreateTask = false;
  }


}
