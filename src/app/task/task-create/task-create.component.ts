import { Component, OnInit, OnChanges, EventEmitter, Input, Output, SimpleChanges } from '@angular/core';
import { Task } from '../../models/task';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-task-create',
  templateUrl: './task-create.component.html',
  styleUrls: ['./task-create.component.css']
})
export class TaskCreateComponent implements OnInit {

  @Input() task: Task = {} as Task;
  @Output() taskChange = new EventEmitter<Task>();
  taskForm: FormGroup;

  constructor() { }

  ngOnInit() {
    console.log(this.task);
    this.taskForm = new FormGroup({
      'id': new FormControl(this.task.id),
      'name': new FormControl(this.task.name, [
        Validators.required,
        Validators.minLength(4)
      ]),
      'state': new FormControl(this.task.state),
      'description': new FormControl(this.task.description),
      'createdAt': new FormControl(this.task.createdAt, [
        Validators.required
      ]),
      'finishedAt': new FormControl(this.task.finishedAt, [
        Validators.required
      ]),
      'performerId': new FormControl(this.task.performerId, [
        Validators.required
      ]),
      'projectId': new FormControl(this.task.projectId, [
        Validators.required
      ]),
    });
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes)
    if (changes.task && this.taskForm) {
      this.taskForm.setValue(changes.task.currentValue)
    }
  }

  savetask() {
    console.log('Emit event');
    let newtask = this.taskForm.value as Task;
    newtask.createdAt = new Date();
    newtask.id=0;
    this.taskChange.emit(newtask);
    this.taskForm.reset();
  }

  revert() {
    this.taskForm.reset();
  }
}
