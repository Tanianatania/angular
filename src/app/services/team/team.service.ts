import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Team } from '../../models/team'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TeamService {

  constructor(private http: HttpClient) { }

  getTeams(): Observable<Team[]> {

    return this.http.get<Team[]>('https://localhost:44354/api/Teams');
  }

  createTeam(newTeam: Team)
  {
    console.log(newTeam);
    this.http.post('https://localhost:44354/api/Teams',newTeam).subscribe(r=>{});
  }

  updateTeam(newTeam: Team)
  {
    console.log(newTeam);
    this.http.put('https://localhost:44354/api/Teams',newTeam).subscribe(r=>{});
  }
}