import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Project } from '../../models/project';
import { Observable } from 'rxjs';
import { HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable({
  providedIn: 'root'
})

export class ProjectService {


  constructor(private http: HttpClient) { }

  getProjects(): Observable<Project[]> {
    console.log("get all projects");
    return this.http.get<Project[]>('https://localhost:44354/api/Projects');
  }

  createProject(newProject: Project)
  {
    console.log(newProject);
    this.http.post('https://localhost:44354/api/Projects',newProject).subscribe(r=>{});
  }

  updateProject(newProject: Project)
  {
    console.log(newProject);
    this.http.put('https://localhost:44354/api/Projects',newProject).subscribe(r=>{});
  }
}
