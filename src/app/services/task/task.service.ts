import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Task } from '../../models/task';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor(private http: HttpClient) { }

  getTasks(): Observable<Task[]> {
    return this.http.get<Task[]>('https://localhost:44354/api/Tasks');
  }

  createTask(newTask: Task)
  {
    console.log(newTask);
    this.http.post('https://localhost:44354/api/Tasks',newTask).subscribe(r=>{});
  }

  updateTask(newTask: Task)
  {
    console.log(newTask);
    this.http.put('https://localhost:44354/api/Tasks',newTask).subscribe(r=>{});
  }
}
