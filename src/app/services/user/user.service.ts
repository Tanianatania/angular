import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../../models/user'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  getUsers(): Observable<User[]> {

    return this.http.get<User[]>('https://localhost:44354/api/Users');
  }

  createUser(newUser: User)
  {
    this.http.post('https://localhost:44354/api/Users',newUser).subscribe(r=>{});
  }

  updateUser(newUser: User)
  {
    console.log(newUser);
    this.http.put('https://localhost:44354/api/Users',newUser).subscribe(r=>{});
  }
}