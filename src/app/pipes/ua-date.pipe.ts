import { DatePipe } from '@angular/common';
import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Pipe({
  name: 'uaDate',
  pure: false
})
export class UaDatePipe implements PipeTransform {

  constructor(private translateService: TranslateService) {
  }

  transform(value: Date, pattern: string = 'longDate'): any {
    const datePipe: DatePipe = new DatePipe('uk');
    return datePipe.transform(value, pattern);
  }

}
