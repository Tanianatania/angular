
import { NgModule } from '@angular/core';
import { UaDatePipe } from './ua-date.pipe';


@NgModule({
  declarations: [
    UaDatePipe
  ],
  imports: [    
  ],
  exports:[UaDatePipe]
})
export class PipeModule { }